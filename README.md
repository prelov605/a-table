# @kck/a-table

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Compiles and minifies library for production
```
npm run build:library
```

### Publish this package on repository
```
npm run publish
```

### Run full cycle of package publishing 
```
npm run test&build&publish
```
