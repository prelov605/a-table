import Vue from 'vue';
import App from './App';
import messageFunctionsMixin from '../stubs/mixins/messageFunctionsMixin';
import store from './store';

Vue.config.productionTip = false;

Vue.mixin(messageFunctionsMixin);

Vue.mixin({
  methods: {
    getMessageByKey(code) {
      return code;
    },
  },
});

new Vue({
  store,
  data: {
    messages: {
      'rowsCounter.of': 'из',
    },
  },
  render: h => h(App),
}).$mount('#app');
